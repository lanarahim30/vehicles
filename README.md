## First
clone this project
```
git clone https://gitlab.com/lanarahim30/vehicles.git

```
## Second
 - copy or rename .env.example to .env
 - configuration your database

## Installation
### Install project dependencies, run this command:
```
composer install
```
### Generate key Application
```
php artisan key:generate

```
### Database Seeder
```
php artisan db:seed

```
### Run For Dokumentation

```
php artisan l5-swagger:generate

```

after generate dokumentation [swagger dokumentasi](http://127.0.0.1:8000/api/documentation#/)

To running test, run thi command

```
./vendor/bin/phpunit
```