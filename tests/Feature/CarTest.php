<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Traits\RefreshDatabaseTransactionLess;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Car;

class CarTest extends TestCase
{
    use RefreshDatabase, RefreshDatabaseTransactionless {
        RefreshDatabaseTransactionless::refreshTestDatabase insteadof RefreshDatabase;
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_success_input_car()
    {
        $response = $this->json('POST', 
                    '/api/login', 
                    [
                        "email" => "test@example.com",
                        "password" => "secret"
                    ]);
       
        $attr['year'] = 2021;
        $attr['color'] = 'black';
        $attr['price'] = 3000000;
        $attr['machine'] = 'v8';
        $attr['capacity'] = 2;
        $attr['type'] = 'sedan';
        $attr['status'] = true;

        $test = $this->json('POST','api/cars',$attr,['Accept' => 'application/json','Authorization' => 'Bearer '.$response['data']['access_token']])
            ->assertStatus(201)
            ->assertJsonStructure([
                'status',
                'message',
                'data' => [
                    '_id',
                    'year',
                    'color',
                    'price',
                    'machine',
                    'capacity',
                    'type',
                    'status'
                ]
            ]);
    }

    public function test_input_car_without_token()
    {
        $attr['year'] = 2021;
        $attr['color'] = 'black';
        $attr['price'] = 3000000;
        $attr['machine'] = 'v8';
        $attr['capacity'] = 2;
        $attr['type'] = 'sedan';
        $attr['status'] = true;

        $test = $this->json('POST','api/cars',$attr,['Accept' => 'application/json'])
            ->assertStatus(401);
    }

    public function test_required_field_create_car()
    {
        $response = $this->json('POST', 
                    '/api/login', 
                    [
                        "email" => "test@example.com",
                        "password" => "secret"
                    ]);

        $test = $this->json('POST','api/cars',['Accept' => 'application/json','Authorization' => 'Bearer '.$response['data']['access_token']])
            ->assertStatus(401);
    }

    public function test_update_car()
    {
        $response = $this->json('POST', 
                        '/api/login', 
                        [
                            "email" => "test@example.com",
                            "password" => "secret"
                        ]);

        $attr['year'] = 2021;
        $attr['color'] = 'black';
        $attr['price'] = 3000000;
        $attr['machine'] = 'v8';
        $attr['capacity'] = 2;
        $attr['type'] = 'sedan';
        $attr['status'] = true;

        $car = Car::create($attr);
    
        $payload = [
            'year' => 2023,
            'color' => 'white',
            'price' => 50000000,
            'machine' => 'v9',
            'capacity' => 4,
            'type' => 'SUV',
            'status' => true
        ];
        
        $test = $this->json('PATCH','api/cars/'.$car->_id,$payload,['Accept' => 'application/json','Authorization' => 'Bearer '.$response['data']['access_token']])
            ->assertStatus(200)
            ->assertJson([
                'status' => 200,
                'message' => 'car updated',
                'data' => [
                    'year' => 2023,
                    'color' => 'white',
                    'price' => 50000000,
                    'machine' => 'v9',
                    'capacity' => 4,
                    'type' => 'SUV',
                    'status' => true
                ]
            ]);
    }

    public function test_delete_car()
    {
        $response = $this->json('POST', 
                        '/api/login', 
                        [
                            "email" => "test@example.com",
                            "password" => "secret"
                        ]);

        $attr['year'] = 2021;
        $attr['color'] = 'black';
        $attr['price'] = 3000000;
        $attr['machine'] = 'v8';
        $attr['capacity'] = 2;
        $attr['type'] = 'sedan';
        $attr['status'] = true;

        $car = Car::create($attr);

        $test = $this->json('DELETE','api/cars/'.$car->_id,[],['Accept' => 'application/json','Authorization' => 'Bearer '.$response['data']['access_token']])
            ->assertStatus(200);
        
    }
}
