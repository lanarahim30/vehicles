<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Traits\RefreshDatabaseTransactionLess;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;

class AuthTest extends TestCase
{
    use RefreshDatabase, RefreshDatabaseTransactionless {
        RefreshDatabaseTransactionless::refreshTestDatabase insteadof RefreshDatabase;
    } 
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_success_login()
    {
        $payload = [
            'email'    => 'test@example.com',
            'password' => 'secret',
        ];

        $user = User::create([
            'email' => 'test@example.com',
            'password' => bcrypt('secret')
        ]);

        $this->json('POST','api/login',$payload,['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure([
                'status',
                'message',
                'data' => [
                    'access_token',
                    'token_type',
                    'expires_in'
                ]
            ]);
    }

    public function test_required_field_login()
    {
        $this->json('POST','api/login',['Accept' => 'application/json'])
            ->assertStatus(401)
            ->assertJson([
                'success' => false,
                'message' => 'Validation errors',
                'errors' => [
                    'email' => [
                        'silahkan masukan email'
                    ],
                    'password' => [
                        'password tidak boleh kosong'
                    ]
                ]
            ]);
    }

    public function test_wrong_format_email_login()
    {
        $payload = [
            'email'    => 'test',
            'password' => 'secret',
        ];

        $this->json('POST','api/login',$payload,['Accept' => 'application/json'])
            ->assertStatus(401)
            ->assertJson([
                'success' => false,
                'message' => 'Validation errors',
                'errors' => [
                    'email' => [
                        'format email salah'
                    ]
                ]
            ]);
    }
}
