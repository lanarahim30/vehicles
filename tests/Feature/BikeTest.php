<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Traits\RefreshDatabaseTransactionLess;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\MotorBike;

class BikeTest extends TestCase
{
    use RefreshDatabase, RefreshDatabaseTransactionless {
        RefreshDatabaseTransactionless::refreshTestDatabase insteadof RefreshDatabase;
    }
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_success_input_bike()
    {
        $response = $this->json('POST', 
                    '/api/login', 
                    [
                        "email" => "test@example.com",
                        "password" => "secret"
                    ]);
       
        $attr['year'] = 2021;
        $attr['color'] = 'black';
        $attr['price'] = 3000000;
        $attr['machine'] = 'v8';
        $attr['transmisi'] = 'manual';
        $attr['suspension'] = 'air';
        $attr['status'] = true;

        $test = $this->json('POST','api/bikes',$attr,['Accept' => 'application/json','Authorization' => 'Bearer '.$response['data']['access_token']])
            ->assertStatus(201)
            ->assertJsonStructure([
                'status',
                'message',
                'data' => [
                    '_id',
                    'year',
                    'color',
                    'price',
                    'machine',
                    'transmisi',
                    'suspension',
                    'status'
                ]
            ]);
    }

    public function test_input_bike_without_token()
    {
        $attr['year'] = 2021;
        $attr['color'] = 'black';
        $attr['price'] = 3000000;
        $attr['machine'] = 'v8';
        $attr['transmisi'] = 'manual';
        $attr['suspension'] = 'air';
        $attr['status'] = true;

        $test = $this->json('POST','api/bikes',$attr,['Accept' => 'application/json'])
            ->assertStatus(401);
    }

    public function test_required_field_create_bike()
    {
        $response = $this->json('POST', 
                    '/api/login', 
                    [
                        "email" => "test@example.com",
                        "password" => "secret"
                    ]);

        $test = $this->json('POST','api/cars',['Accept' => 'application/json','Authorization' => 'Bearer '.$response['data']['access_token']])
            ->assertStatus(401);
    }

    public function test_update_bike()
    {
        $response = $this->json('POST', 
                        '/api/login', 
                        [
                            "email" => "test@example.com",
                            "password" => "secret"
                        ]);

        $attr['year'] = 2021;
        $attr['color'] = 'black';
        $attr['price'] = 3000000;
        $attr['machine'] = 'v8';
        $attr['transmisi'] = 'manual';
        $attr['suspension'] = 'air';
        $attr['status'] = true;

        $bike = MotorBike::create($attr);
    
        $payload = [
            'year' => 2023,
            'color' => 'white',
            'price' => 50000000,
            'machine' => 'v9',
            'transmisi' => 'matic',
            'suspension' => 'keras',
            'status' => true
        ];
        
        $test = $this->json('PATCH','api/bikes/'.$bike->_id,$payload,['Accept' => 'application/json','Authorization' => 'Bearer '.$response['data']['access_token']])
            ->assertStatus(200)
            ->assertJson([
                'status' => 200,
                'message' => 'motor bike updated',
                'data' => [
                    'year' => 2023,
                    'color' => 'white',
                    'price' => 50000000,
                    'machine' => 'v9',
                    'transmisi' => 'matic',
                    'suspension' => 'keras',
                    'status' => true
                ]
            ]);
    }

    public function test_delete_bike()
    {
        $response = $this->json('POST', 
                        '/api/login', 
                        [
                            "email" => "test@example.com",
                            "password" => "secret"
                        ]);

        $attr['year'] = 2021;
        $attr['color'] = 'black';
        $attr['price'] = 3000000;
        $attr['machine'] = 'v8';
        $attr['transmisi'] = 'manual';
        $attr['suspension'] = 'air';
        $attr['status'] = true;

        $bike = MotorBike::create($attr);

        $test = $this->json('DELETE','api/bikes/'.$bike->_id,[],['Accept' => 'application/json','Authorization' => 'Bearer '.$response['data']['access_token']])
            ->assertStatus(200);
        
    }
}
