<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\{
    CarController,
    MotorBikeController,
    VehicleController,
    AuthController
};

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('login',[AuthController::class,'login']);

Route::middleware('jwt.verify')->group(function(){
    Route::get('refresh',[AuthController::class,'refresh']);
    Route::get('profile',[AuthController::class,'me']);
    Route::post('logout',[AuthController::class,'logout']);

    Route::prefix('cars')->group(function(){
        Route::get('/',[CarController::class,'cars']);
        Route::post('/',[CarController::class,'store']);
        Route::patch('/{id}',[CarController::class,'update']);
        Route::delete('/{id}',[CarController::class,'delete']);
    });
    
    Route::prefix('bikes')->group(function(){
        Route::get('/',[MotorBikeController::class,'bikes']); 
        Route::post('/',[MotorBikeController::class,'store']);
        Route::patch('/{id}',[MotorBikeController::class,'update']);
        Route::delete('/{id}',[MotorBikeController::class,'delete']);
    });
    
    Route::post('order',[VehicleController::class,'order']);
    Route::get('stock',[VehicleController::class,'stock']);
    Route::get('sales',[VehicleController::class,'sales']);
    Route::get('report',[VehicleController::class,'report']);
});
