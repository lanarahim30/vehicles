<?php
namespace App\Repositories;
use App\Models\MotorBike;
use Illuminate\Support\Collection;

class MotorBikeRepository {

    public function getBikes() : Collection
    {
        return MotorBike::get();
    }

    public function insertMotorBike(array $data) : MotorBike
    {
        $bike = New MotorBike();
        
        $bike->setYear($data['year']);
        $bike->setColor($data['color']);
        $bike->setPrice($data['price']);
        $bike->setMachine($data['machine']);
        $bike->setSuspension($data['suspension']);
        $bike->setTransmisi($data['transmisi']);
        $bike->setStatus($data['status']);
        $bike->save();
        
        return $bike;
    
    }

    public function updateMotorBike(array $data, $id) : MotorBike
    {
        $bike = MotorBike::find($id);

        $bike->setYear($data['year']);
        $bike->setColor($data['color']);
        $bike->setPrice($data['price']);
        $bike->setMachine($data['machine']);
        $bike->setSuspension($data['suspension']);
        $bike->setTransmisi($data['transmisi']);
        $bike->setStatus($data['status']);
        $bike->save();
        
        return $bike;
    }

    public function deleteMotorBike($id): MotorBike
    {
        $bike = MotorBike::find($id);
        
        $bike->delete();

        return $bike;
    }

    public function orderMotorBike($id)
    {
        $bike = MotorBike::where('_id',$id)->where('status',true)->first();

        if($bike)
        {
            $bike->setStatus(false);
            $bike->save();

            return $bike;
        }
        
        return false;
    }

    public function stockMotorBike() : int
    {
        return MotorBike::where('status',true)->count();
    }
    public function motorBikeSales()
    {
        return MotorBike::where('status',false)->count();
    }
}