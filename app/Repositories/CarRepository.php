<?php
namespace App\Repositories;
use App\Models\Car;
use Illuminate\Support\Collection;

class CarRepository {

    public function getCars() : Collection
    {
        return Car::get();
    }

    public function insertCar(array $data) : Car
    {
        $car = New Car();
        
        $car->setYear($data['year']);
        $car->setColor($data['color']);
        $car->setPrice($data['price']);
        $car->setMachine($data['machine']);
        $car->setCapacity($data['capacity']);
        $car->setType($data['type']);
        $car->setStatus($data['status']);
        $car->save();
        
        return $car;
    
    }

    public function updateCar(array $data, $id) : Car
    {
        $car = Car::find($id);

        $car->setYear($data['year']);
        $car->setColor($data['color']);
        $car->setPrice($data['price']);
        $car->setMachine($data['machine']);
        $car->setCapacity($data['capacity']);
        $car->setType($data['type']);
        $car->setStatus($data['status']);
        $car->save();
        
        return $car;
    }

    public function deleteCar($id): Car
    {
        $car = Car::find($id);
        
        $car->delete();

        return $car;
    }

    public function orderCar($id)
    {
        $car = Car::where('_id',$id)->where('status',true)->first();
        
        if($car)
        {
            $car->setStatus(false);
            $car->save();

            return $car;
        }
       
        return false;
    }

    public function stockCar() : int
    {
        return Car::where('status',true)->count();
    }
    public function carSales()
    {
        return Car::where('status',false)->count();
    }
}