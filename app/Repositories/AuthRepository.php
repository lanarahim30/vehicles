<?php
namespace App\Repositories;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class AuthRepository {

    public function login($request)
    {
        $credentials = $request->only('email','password');

        $token = Auth::attempt($credentials);

        return $token;
    }
}