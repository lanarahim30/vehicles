<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Exceptions\HttpResponseException;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function failedValidation(Validator $validator) : JsonResponse
    {

        throw new HttpResponseException(response()->json([

            'success'   => false,

            'message'   => 'Validation errors',

            'errors'      => $validator->errors()

        ],400));

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'type' => 'required|in:car,motorbike',
            'vehicle_id' => $this->type == 'car' ? 'required|exists:mongodb.cars,_id' : 'required|exists:mongodb.motor_bikes,_id',
        ];
    }

    public function messages()
    {
        return [
            'type.required' => 'type tidak boleh kosong',
            'type.in' => 'type hanya car dan motorbike',
            'vehicle_id.required' => 'id tidak boleh kosong',
            'vehicle_id.exists' => 'id tidak ditemukan'
        ];
    }
}
