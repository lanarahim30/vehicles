<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Exceptions\HttpResponseException;

class CarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function failedValidation(Validator $validator) : JsonResponse
    {

        throw new HttpResponseException(response()->json([

            'success'   => false,

            'message'   => 'Validation errors',

            'errors'      => $validator->errors()

        ],400));

    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'year' => 'required|numeric',
            'color' => 'required',
            'price' => 'required|numeric',
            'machine' => 'required',
            'capacity' => 'required|numeric',
            'type' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'year.required' => 'silahkan masukan tahun',
            'year.numeric' => 'tahun tidak harus numerik',
            'color.required' => 'silahkan masukan warna',
            'price.required' => 'silahkan masukan harga',
            'price.numeric' => 'format harga harus numerik',
            'capacity.required' => 'silahkan masukan kapasitas',
            'capacity.numeric' => 'format kapasitas harus numerik',
            'type.required' => 'silahkan masukan tipe',
        ];
    }
}
