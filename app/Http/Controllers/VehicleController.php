<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\VehicleService;
use App\Http\Requests\OrderRequest;

class VehicleController extends Controller
{
    //
    public function __construct
    (
        protected VehicleService $vehicleService,
    ){}

    /**
    *@OA\Post(
        *   path="/api/order",
        *   tags={"Vehicle"},
        *   summary="Order Vehicle",
        *   operationId="order",
        *   security={{"bearer_token":{}}},
        *   description="Order Vehile, type(car,motorbike)",
        *      @OA\RequestBody(
        *         required=true,
        *         @OA\JsonContent(
        *            required={"type", "vehicle_id"},
        *            @OA\Property(property="type", type="string", format="string", example="car"),
        *            @OA\Property(property="vehicle_id", type="string", format="string", example=""),
        *         ),
        *      ),
        *   @OA\Response(
        *      response=200,
        *      description="Success",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   ),
         *   @OA\Response(
        *      response=201,
        *      description="created",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   ),
        *   @OA\Response(
        *      response=403,
        *      description="Forbidden",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   ),
        *   @OA\Response(
        *      response=422,
        *      description="Validation Error",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   )
        *)
    */
    public function order(OrderRequest $request)
    {
        $attr['type'] = $request->type;
        $attr['vehicle_id'] = $request->vehicle_id;

        $order = $this->vehicleService->order($attr);

        return $order;
    }

    /**
    *@OA\Get(
        *   path="/api/stock",
        *   tags={"Vehicle"},
        *   summary="Stock Vehicle",
        *   operationId="stock",
        *   security={{"bearer_token":{}}},
        *   @OA\Response(
        *      response=200,
        *      description="Success",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   ),
        *   @OA\Response(
        *      response=403,
        *      description="Forbidden",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   ),
        *   @OA\Response(
        *      response=422,
        *      description="Validation Error",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   )
        *)
    */
    public function stock()
    {
       return $this->vehicleService->vehicleStock();
    }

    /**
    *@OA\Get(
        *   path="/api/sales",
        *   tags={"Vehicle"},
        *   summary="Sales Vehicle",
        *   operationId="sales",
        *   security={{"bearer_token":{}}},
        *   @OA\Response(
        *      response=200,
        *      description="Success",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   ),
        *   @OA\Response(
        *      response=403,
        *      description="Forbidden",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   ),
        *   @OA\Response(
        *      response=422,
        *      description="Validation Error",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   )
        *)
    */
    public function sales()
    {
       return $this->vehicleService->vehicleSales();
    }

    /**
    *@OA\Get(
        *   path="/api/report",
        *   tags={"Vehicle"},
        *   summary="Report Sales Vehicle",
        *   operationId="report",
        *   security={{"bearer_token":{}}},
        *   @OA\Response(
        *      response=200,
        *      description="Success",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   ),
        *   @OA\Response(
        *      response=403,
        *      description="Forbidden",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   ),
        *   @OA\Response(
        *      response=422,
        *      description="Validation Error",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   )
        *)
    */
    public function report()
    {
       return $this->vehicleService->reportSales();
    }
}
