<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\AuthService;
use App\Http\Requests\LoginRequest;

class AuthController extends Controller
{
    //
    public function __construct(protected AuthService $authService)
    {}

    /**
    *@OA\Post(
        *   path="/api/login",
        *   tags={"User"},
        *   summary="Login User",
        *   operationId="login",
        *   @OA\Parameter(
        *      name="email",
        *      in="query",
        *      @OA\Schema(
        *           type="string"
        *      )
        *   ),
        *   @OA\Parameter(
        *      name="password",
        *      in="query",
        *      @OA\Schema(
        *          type="string",
        *          format="password"
        *      )
        *   ),
        *   @OA\Response(
        *      response=200,
        *      description="Success",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   ),
        *   @OA\Response(
        *      response=403,
        *      description="Forbidden",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   ),
        *   @OA\Response(
        *      response=422,
        *      description="Validation Error",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   )
        *)
    */
    public function login(LoginRequest $request)
    {
        $login = $this->authService->login($request);

        return $login;
    }

    /**
    *@OA\Get(
        *   path="/api/profile",
        *   tags={"User"},
        *   summary="User Login",
        *   operationId="tokenme",
        *   security={{"bearer_token":{}}},
        *   @OA\Response(
        *      response=200,
        *      description="Success",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   ),
        *   @OA\Response(
        *      response=403,
        *      description="Forbidden",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   ),
        *   @OA\Response(
        *      response=422,
        *      description="Validation Error",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   )
        *)
    */
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
    *@OA\Post(
        *   path="/api/logout",
        *   tags={"User"},
        *   summary="User Logout",
        *   operationId="logout",
        *   security={{"bearer_token":{}}},
        *   @OA\Response(
        *      response=200,
        *      description="Success",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   ),
        *   @OA\Response(
        *      response=403,
        *      description="Forbidden",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   ),
        *   @OA\Response(
        *      response=422,
        *      description="Validation Error",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   )
        *)
    */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
    *@OA\Get(
        *   path="/api/refresh",
        *   tags={"User"},
        *   summary="Refresh Token Login",
        *   operationId="refresh",
        *   security={{"bearer_token":{}}},
        *   @OA\Response(
        *      response=200,
        *      description="Success",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   ),
        *   @OA\Response(
        *      response=403,
        *      description="Forbidden",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   ),
        *   @OA\Response(
        *      response=422,
        *      description="Validation Error",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   )
        *)
    */
    public function refresh()
    {
        return $this->authService->refresh();
    }

}
