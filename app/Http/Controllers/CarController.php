<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\CarService;
use App\Http\Requests\CarRequest;

class CarController extends Controller
{
    //
    public function __construct(protected CarService $carService){}

    /**
    *@OA\Get(
        *   path="/api/cars",
        *   tags={"Car"},
        *   summary="List Car",
        *   operationId="cars",
        *   security={{"bearer_token":{}}},
        *   @OA\Response(
        *      response=200,
        *      description="Success",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   ),
        *   @OA\Response(
        *      response=403,
        *      description="Forbidden",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   ),
        *   @OA\Response(
        *      response=422,
        *      description="Validation Error",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   )
        *)
    */
    public function cars()
    {
        return $this->carService->getCars();
    }

    /**
    *@OA\Post(
        *   path="/api/cars",
        *   tags={"Car"},
        *   summary="Add Car",
        *   operationId="store",
        *   security={{"bearer_token":{}}},
        *      @OA\RequestBody(
        *         required=true,
        *         @OA\JsonContent(
        *            required={"year", "color", "price","machine","capacity","type"},
        *            @OA\Property(property="year", type="integer", format="integer", example="2020"),
        *            @OA\Property(property="color", type="string", format="string", example="black"),
        *            @OA\Property(property="price", type="integer", format="integer", example="200000000"),
        *            @OA\Property(property="machine", type="string", format="string", example="v8"),
        *            @OA\Property(property="capacity", type="integer", format="integer", example="4"),
        *            @OA\Property(property="type", type="string", format="string", example="SUV"),
        *            @OA\Property(property="status", type="boolean", format="boolean", example="true"),
        *         ),
        *      ),
        *   @OA\Response(
        *      response=200,
        *      description="Success",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   ),
         *   @OA\Response(
        *      response=201,
        *      description="created",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   ),
        *   @OA\Response(
        *      response=403,
        *      description="Forbidden",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   ),
        *   @OA\Response(
        *      response=422,
        *      description="Validation Error",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   )
        *)
    */
    public function store(CarRequest $request)
    {
        $attr['year'] = $request->year;
        $attr['color'] = $request->color;
        $attr['price'] = $request->price;
        $attr['machine'] = $request->machine;
        $attr['capacity'] = $request->capacity;
        $attr['type'] = $request->type;
        $attr['status'] = $request->status ?? true;

        $car = $this->carService->insertCar($attr);

        return $car;
    }

    /**
    *@OA\Patch(
        *   path="/api/cars/{id}",
        *   tags={"Car"},
        *   summary="Update Car",
        *   operationId="update",
        *   security={{"bearer_token":{}}},
        *   @OA\Parameter(
        *      name="id",
        *      in="path",
        *      @OA\Schema(
        *           type="string"
        *      )
        *   ),
        *   @OA\RequestBody(
        *         required=true,
        *         @OA\JsonContent(
        *            required={"year", "color", "price","machine","capacity","type"},
        *            @OA\Property(property="year", type="integer", format="integer", example="2020"),
        *            @OA\Property(property="color", type="string", format="string", example="black"),
        *            @OA\Property(property="price", type="integer", format="integer", example="200000000"),
        *            @OA\Property(property="machine", type="string", format="string", example="v8"),
        *            @OA\Property(property="capacity", type="integer", format="integer", example="4"),
        *            @OA\Property(property="type", type="string", format="string", example="SUV"),
        *            @OA\Property(property="status", type="boolean", format="boolean", example="true"),
        *       ),
        *   ),
        *   @OA\Response(
        *      response=200,
        *      description="Success",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   ),
         *   @OA\Response(
        *      response=201,
        *      description="created",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   ),
        *   @OA\Response(
        *      response=403,
        *      description="Forbidden",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   ),
        *   @OA\Response(
        *      response=422,
        *      description="Validation Error",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   )
        *)
    */

    public function update(CarRequest $request, $id)
    {
        $attr['year'] = $request->year;
        $attr['color'] = $request->color;
        $attr['price'] = $request->price;
        $attr['machine'] = $request->machine;
        $attr['capacity'] = $request->capacity;
        $attr['type'] = $request->type;
        $attr['status'] = $request->status ?? true;

        $car = $this->carService->updateCar($attr,$id);

        return $car;
    }

    /**
    *@OA\Delete(
        *   path="/api/cars/{id}",
        *   tags={"Car"},
        *   summary="Delete Car",
        *   operationId="delete",
        *   security={{"bearer_token":{}}},
        *   @OA\Parameter(
        *      name="id",
        *      in="path",
        *      @OA\Schema(
        *           type="string"
        *      )
        *   ),
        *   @OA\Response(
        *      response=200,
        *      description="Success",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   ),
         *   @OA\Response(
        *      response=201,
        *      description="created",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   ),
        *   @OA\Response(
        *      response=403,
        *      description="Forbidden",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   ),
        *   @OA\Response(
        *      response=422,
        *      description="Validation Error",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   )
        *)
    */
    public function delete($id)
    {
        $car = $this->carService->deleteCar($id);

        return $car;
    }
}
