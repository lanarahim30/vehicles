<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\MotorBikeService;
use App\Http\Requests\MotorBikeRequest;

class MotorBikeController extends Controller
{
    //
    public function __construct(protected MotorBikeService $motorBikeService){}


    /**
    *@OA\Get(
        *   path="/api/bikes",
        *   tags={"Bike"},
        *   summary="List bike",
        *   operationId="bikes",
        *   security={{"bearer_token":{}}},
        *   @OA\Response(
        *      response=200,
        *      description="Success",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   ),
        *   @OA\Response(
        *      response=403,
        *      description="Forbidden",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   ),
        *   @OA\Response(
        *      response=422,
        *      description="Validation Error",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   )
        *)
    */
    public function bikes()
    {
        return $this->motorBikeService->getBikes();
    }

    /**
    *@OA\Post(
        *   path="/api/bikes",
        *   tags={"Bike"},
        *   summary="Add Bike",
        *   operationId="storebike",
        *   security={{"bearer_token":{}}},
        *      @OA\RequestBody(
        *         required=true,
        *         @OA\JsonContent(
        *            required={"year", "color", "price","machine","transmisi","suspension"},
        *            @OA\Property(property="year", type="integer", format="integer", example="2020"),
        *            @OA\Property(property="color", type="string", format="string", example="black"),
        *            @OA\Property(property="price", type="integer", format="integer", example="200000000"),
        *            @OA\Property(property="machine", type="string", format="string", example="v8"),
        *            @OA\Property(property="transmisi", type="string", format="string", example="manual"),
        *            @OA\Property(property="suspension", type="string", format="string", example="air suspension"),
        *            @OA\Property(property="status", type="boolean", format="boolean", example="true"),
        *         ),
        *      ),
        *   @OA\Response(
        *      response=200,
        *      description="Success",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   ),
         *   @OA\Response(
        *      response=201,
        *      description="created",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   ),
        *   @OA\Response(
        *      response=403,
        *      description="Forbidden",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   ),
        *   @OA\Response(
        *      response=422,
        *      description="Validation Error",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   )
        *)
    */
    public function store(MotorBikeRequest $request)
    {
        $attr['year'] = $request->year;
        $attr['color'] = $request->color;
        $attr['price'] = $request->price;
        $attr['machine'] = $request->machine;
        $attr['transmisi'] = $request->transmisi;
        $attr['suspension'] = $request->suspension;
        $attr['status'] = $request->status ?? true;

        $motorBike = $this->motorBikeService->insertMotorBike($attr);

        return $motorBike;
    }

     /**
    *@OA\Patch(
        *   path="/api/bikes/{id}",
        *   tags={"Bike"},
        *   summary="Update Bike",
        *   operationId="updatebike",
        *   security={{"bearer_token":{}}},
        *   @OA\Parameter(
        *      name="id",
        *      in="path",
        *      @OA\Schema(
        *           type="string"
        *      )
        *     ),
        *      @OA\RequestBody(
        *         required=true,
        *         @OA\JsonContent(
        *            required={"year", "color", "price","machine","transmisi","suspension"},
        *            @OA\Property(property="year", type="integer", format="integer", example="2020"),
        *            @OA\Property(property="color", type="string", format="string", example="black"),
        *            @OA\Property(property="price", type="integer", format="integer", example="200000000"),
        *            @OA\Property(property="machine", type="string", format="string", example="v8"),
        *            @OA\Property(property="transmisi", type="string", format="string", example="manual"),
        *            @OA\Property(property="suspension", type="string", format="string", example="air suspension"),
        *            @OA\Property(property="status", type="boolean", format="boolean", example="true"),
        *         ),
        *      ),
        *   @OA\Response(
        *      response=200,
        *      description="Success",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   ),
         *   @OA\Response(
        *      response=201,
        *      description="created",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   ),
        *   @OA\Response(
        *      response=403,
        *      description="Forbidden",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   ),
        *   @OA\Response(
        *      response=422,
        *      description="Validation Error",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   )
        *)
    */
    public function update(MotorBikeRequest $request,$id)
    {
        $attr['year'] = $request->year;
        $attr['color'] = $request->color;
        $attr['price'] = $request->price;
        $attr['machine'] = $request->machine;
        $attr['transmisi'] = $request->transmisi;
        $attr['suspension'] = $request->suspension;
        $attr['status'] = $request->status ?? true;

        $motorBike = $this->motorBikeService->updateMotorBike($attr,$id);

        return $motorBike;
    }

    /**
    *@OA\Delete(
        *   path="/api/bikes/{id}",
        *   tags={"Bike"},
        *   summary="Delete Bike",
        *   operationId="deletebike",
        *   security={{"bearer_token":{}}},
        *   @OA\Parameter(
        *      name="id",
        *      in="path",
        *      @OA\Schema(
        *           type="string"
        *      )
        *   ),
        *   @OA\Response(
        *      response=200,
        *      description="Success",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   ),
         *   @OA\Response(
        *      response=201,
        *      description="created",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   ),
        *   @OA\Response(
        *      response=403,
        *      description="Forbidden",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   ),
        *   @OA\Response(
        *      response=422,
        *      description="Validation Error",
        *      @OA\MediaType(
        *           mediaType="application/json",
        *      )
        *   )
        *)
    */
    public function delete($id)
    {
        $motorBike = $this->motorBikeService->deleteMotorBike($id);

        return $motorBike;
    }
}
