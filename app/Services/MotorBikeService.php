<?php
namespace App\Services;
use App\Repositories\MotorBikeRepository;
use App\Traits\ApiHelpers;

class MotorBikeService {

    use ApiHelpers;

    public function __construct(protected MotorBikeRepository $repository){}

    public function getBikes()
    {
        return $this->onSuccess($this->repository->getBikes(),'all bikes');
    }

    public function insertMotorBike(array $data)
    {
       
        $motorBike = $this->repository->insertMotorBike($data);

        if(!$motorBike)
        {
            throw new \Exception('error insert data');
        }

        return  $this->onSuccess($motorBike,'motor bike created',201);
    }

    public function updateMotorBike(array $data,$id)
    {
       
        $motorBike = $this->repository->updateMotorBike($data,$id);

        if(!$motorBike)
        {
            throw new \Exception('error update data');
        }

        return  $this->onSuccess($motorBike,'motor bike updated');
    }

    public function deleteMotorBike($id)
    {
       
        $motorBike = $this->repository->deleteMotorBike($id);

        if(!$motorBike)
        {
            throw new \Exception('error delete data');
        }

        return $this->onSuccess($motorBike,'motor bike deleted');
    }

    public function orderMotorBike($id)
    {
        $bike = $this->repository->orderMotorBike($id);

        if(!$bike)
        {
            throw new \Exception('motor tidak ditemukan atau sudah terjual');
        }

        return $bike;
    }
}