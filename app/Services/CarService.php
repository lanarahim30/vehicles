<?php
namespace App\Services;
use App\Repositories\CarRepository;
use App\Traits\ApiHelpers;

class CarService {

    use ApiHelpers;

    public function __construct(protected CarRepository $repository)
    {
    }
    
    public function getCars()
    {
        return  $this->onSuccess($this->repository->getCars(),'all cars');
    }

    public function insertCar(array $data)
    {
       
        $car = $this->repository->insertCar($data);

        if(!$car)
        {
            throw new \Exception('error insert data');
        }

        return $this->onSuccess($car,'car created',201);
    }

    public function updateCar(array $data,$id)
    {
       
        $car = $this->repository->updateCar($data,$id);

        if(!$car)
        {
            throw new \Exception('error update data');
        }

        return $this->onSuccess($car,'car updated');
    }

    public function deleteCar($id)
    {
       
        $car = $this->repository->deleteCar($id);

        if(!$car)
        {
            throw new \Exception('error delete data');
        }

        return $this->onSuccess($car,'car deleted');
    }

    public function orderCar($id)
    {
        $car = $this->repository->orderCar($id);

        if(!$car)
        {
            throw new \Exception('mobil tidak ditemukan atau sudah terjual');
        }

        return $this->onSuccess($car,'car ordered');
    }
    
}