<?php
namespace App\Services;
use App\Repositories\AuthRepository;
use App\Traits\ApiHelpers;

class AuthService {

    use ApiHelpers;

    public function __construct(protected AuthRepository $repository){}

    public function login($request)
    {
        $token = $this->repository->login($request);

        if(!$token)
        {
            throw new \Exception('Unauthorized');
        }

        $data = [
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ];

        return $this->onSuccess($data,'login success');
    }

    public function refresh()
    {
        $data = [
            'refresh_token' => auth()->refresh(),
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ];

        return $this->onSuccess($data,'refresh token success');
    }
}