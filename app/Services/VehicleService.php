<?php
namespace App\Services;
use App\Repositories\{CarRepository,MotorBikeRepository};
use App\Traits\ApiHelpers;

class VehicleService {
    use ApiHelpers;

    public function __construct(
        protected MotorBikeRepository $motorBikeRepository,
        protected CarRepository $carRepository
    ){}

    public function vehicleStock()
    {
        $data = [
            'car' => $this->carRepository->stockCar(),
            'motor_bike' => $this->motorBikeRepository->stockMotorBike()
        ];

        return $this->onSuccess($data,'vehicle stock');
    }

    public function vehicleSales()
    {
        $data = [
            'car' => $this->carRepository->carSales(),
            'motor_bike' => $this->motorBikeRepository->motorBikeSales()
        ];

        return $this->onSuccess($data,'vehicle sales');
    }

    public function reportSales()
    {
        $data = [
            'car' => [
                'sales' => $this->carRepository->carSales(),
                'stock' => $this->carRepository->stockCar()
            ],
            'motor_bike' => [
                'sales' => $this->motorBikeRepository->motorBikeSales(),
                'stock' => $this->motorBikeRepository->stockMotorBike()
            ]
        ];

        return $this->onSuccess($data,'report sale vehicle');
    }

    public function order(array $data)
    {
        $type = $data['type'];
        $id = $data['vehicle_id'];
       
        switch ($type) {
            case 'car':
                $order = $this->carRepository->orderCar($id);
                break;
            case 'motorbike':
                $order =  $this->motorBikeRepository->orderMotorBike($id);
                break;
            default:
                $order = 'type tidak ada';
                break;
        }
       
        if($order == false)
        {
            throw new \Exception('kendaraan tidak ditemukan atau sudah terjual');
        }

        return $this->onSuccess($order,'order vehicle success');
    }
}