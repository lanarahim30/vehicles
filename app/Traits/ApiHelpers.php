<?php
namespace App\Traits;
use Illuminate\Http\JsonResponse;

trait ApiHelpers{

    protected function onSuccess($data, string $message = '', int $code = 200): JsonResponse
    {
        return response()->json([
            'status' => $code,
            'message' => $message,
            'data' => $data,
        ], $code);
    }

    protected function onError(int $code, $errors,$msg = ''): JsonResponse
    {
        return response()->json([
            'status' => $code,
            'errors' => $errors,
            'message' => isset($msg) && $msg !== '' ? $msg : 'Validation Errors',
        ], $code);
    }
}
