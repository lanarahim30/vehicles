<?php
declare(strict_types=1);
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MotorBike extends Vehicle
{
    public  $machine;
    public  $sunpension;
    public  $transmisi;

    protected $guarded = [];
    
    public function setMachine($machineV)
    {
        $this->attributes['machine'] = $machineV;
    }

    public function setSuspension($sunpensionV)
    {
        $this->attributes['suspension'] = $sunpensionV;
    }

    public function setTransmisi($transmisiV)
    {
        $this->attributes['transmisi'] = $transmisiV;
    }
}
