<?php
declare(strict_types=1);
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Car extends Vehicle
{
    public string $machine;
    public string $capacity;
    public string $type;
    
    protected $guarded = [];

    public function setMachine($machineV)
    {
        $this->attributes['machine'] = $machineV;
    }

    public function setCapacity($capacityV)
    {
        $this->attributes['capacity'] = $capacityV;
    }

    public function setType($typeV)
    {
        $this->attributes['type'] = $typeV;
    }
}
