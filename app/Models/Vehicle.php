<?php
declare(strict_types=1);
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Vehicle extends Eloquent
{
    use HasFactory;

    protected $connection = 'mongodb';
    protected $collections = 'vehicles';

    public $year;
    public string $color;
    public float $price;
    public bool $status;

    public function setYear($yearV)
    {
        $this->attributes['year'] = $yearV;
    }

    public function setColor($colorV)
    {
        $this->attributes['color'] = $colorV;
    }

    public function setPrice($priceV)
    {
        $this->attributes['price'] = $priceV;
    }

    public function setStatus($statusv)
    {
        $this->attributes['status'] = $statusv;
    }


}