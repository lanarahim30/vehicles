<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Car;

class VehicleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Car::create([
            'year' => 2023,
            'color' => 'black',
            'price' => 1000000000,
            'status' => true,
            'machine' => 'v9',
            'capacity' => 4,
            'type' => 'suv'
        ]);
    }
}
